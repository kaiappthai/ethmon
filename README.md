# EthMon

Monitor ETH transactions via JSON-RPC API using Go.

## Requirements

-   Go 1.18 or higher.
-   MySQL (5.6+) with database name you define in [configuration](#configuration) (default is eth_wallet_monitor).

## Usage

Simply clone repository and please check [configuration](#configuration) for mySQL and other settings before run go.

    $ cd directory/to/ethmon
    $ go run ./

by go run ./ will check EthRPC server and MySQL database and then create table "transaction" if not exist and then start API server. API consist of

### Fetch transactions

    POST  /api/fetchTransactionsByAccounts

Fetch Ethereum transactions from JSON-RPC API with multiple addresses and ranges of block and save to database.

*request JSON example*

    {
      "wallets":  [
        {
          "wallet":  "0x28c6c06298d514db089934071355e5743bf21d60",
          "startBlock":  17065470,
          "endBlock":  17065471
        }
      ],
      "saveToDB":  true
    }

\* database will not save duplicate transaction data

### Get transactions

    GET  /api/transactionsByAccount

Get transactions of specific address and range of block from database.

*request JSON example*

    {
          "addr":  "0x28c6c06298d514db089934071355e5743bf21d60",
          "startBlock":  17065470,
          "endBlock":  17065471
    }

### Remove transactions

    DELETE 	/api/transactionsByAccount

Remove transactions of specific address and range of block from database.

*request JSON example*

    {
          "addr":  "0x28c6c06298d514db089934071355e5743bf21d60",
          "startBlock":  17065470,
          "endBlock":  17065471
    }
\* this API is for test remove and insert data into database.

Postman user can use EthMon.postman_collection.json file to import collection.

### Unit Test

You can test fetch, insert, retrieve transactions by

    $ go test

## Configuration

There are 3 config json files that need to check

/ethrpc/config.json

    {
      "server": "https://eth.llamarpc.com"
    }

Ethereum JSON-RPC API server. You can get more list at https://chainlist.org/chain/1

/mysql/config.json

    {
      "db_host": "localhost",
      "db_port": 3306,
      "database": "eth_wallet_monitor",
      "username": "testuser",
      "password": "password1234",
      "use_ssl": false,
      "ca_cert": "mysql/cert/ca-cert.pem",
      "client_cert": "mysql/cert/client-cert.pem",
      "client_key": "mysql/cert/client-key.pem"
    }

By using SSL, you need to have ca-cert.pem, client-cert.pem and client-key.pem in define path.

/api/config.json

    {
      "api_port": 8080,
      "api_path": "/api"
    }

You can define port and path of rest API.

### Approach and Design Decision

In order to monitor Ethereum incoming and outgoing transactions properly, system need to fetch transactions in every blocks. And each block take 30 seconds to 1 minute to fetch. For run check on specific addresses on specify block on Ethereum API directly, I decide to have batch wallet addresses request in order to run through each blocks once and check for multiple addresses.

And with 30-60 sec per block scan and support large no of addresses, I decide to save to database immediately, rather than store on object or json. To minimize using or memory.

### Long-Term Approach

If system need to check all transactions of multiple wallets every day, I suggest to build index database table, which one-time fetch and store block number, transaction hash, to address and from address of every transactions of every block. So once you need to fetch incoming and outgoing transactions of specific wallet, you can fetch index from database instead of Ethereum RPC API, which much faster. But it need about 20 Mb of database space.