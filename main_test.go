package main

import (
	"encoding/json"
	"ethmon/ethrpc"
	"ethmon/monitor"
	"ethmon/mysql"
	"fmt"
	"reflect"
	"testing"
)

func TestFetchEthRPC(t *testing.T) {
	var addr string = "0x28c6c06298d514db089934071355e5743bf21d60"
	var startBlock int = 17065470
	var endBlock int = 17065471
	var correct_result = []string{
		"0x43f2bb0d4b852bf40410f286bf74830d874942c6826a5a51e5d416f146ab81b4",
		"0x4964cff8ce520fc6f76b2052be65b0c477583117485cd3ddb0e7d9b6c1bc84e6",
		"0xdddd39b219d88f0faa748b997a3976fc0cc80a4aaefcf3b3b4182f253ab960a5",
		"0xdf8e3e3afe0361d69103ad6474c62147a7a6840742d32e616b04703d976d4da9"}

	db, err := mysql.NewMySQLConnect()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// delete old data
	err = mysql.DeleteTransactionsByWallet(db, addr, startBlock, endBlock)
	if err != nil {
		panic(err)
	}

	fmt.Println("")
	fmt.Println("Fetch transactions from Ethereum JSON RPC API and save to database")
	fmt.Println("wallet:", addr)
	fmt.Println("startBlock: ", startBlock)
	fmt.Println("endBlock: ", endBlock)
	fmt.Println("")
	result, _ := monitor.FetchTransactionsByAccount(addr, startBlock, endBlock, true, true)
	fmt.Println("")
	if result == nil {
		t.Fatalf(`No result.`)
		return
	}
	var txs []string
	for _, item := range result {
		data := new(ethrpc.Transaction)
		json.Unmarshal([]byte(item.TransactionData), &data)
		txs = append(txs, data.Hash)
	}
	if !reflect.DeepEqual(txs, correct_result) {
		t.Fatalf(`Result Incorrect.`)
	}
	fmt.Println("Result Match")
	fmt.Println("---------------")
	fmt.Println("")
}

func TestDBQuery(t *testing.T) {
	var addr string = "0x28c6c06298d514db089934071355e5743bf21d60"
	var startBlock int = 17065470
	var endBlock int = 17065471
	var correct_result = []string{
		"0x43f2bb0d4b852bf40410f286bf74830d874942c6826a5a51e5d416f146ab81b4",
		"0x4964cff8ce520fc6f76b2052be65b0c477583117485cd3ddb0e7d9b6c1bc84e6",
		"0xdddd39b219d88f0faa748b997a3976fc0cc80a4aaefcf3b3b4182f253ab960a5",
		"0xdf8e3e3afe0361d69103ad6474c62147a7a6840742d32e616b04703d976d4da9"}

	db, err := mysql.NewMySQLConnect()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	fmt.Println("Query transactions from database")
	fmt.Println("wallet:", addr)
	fmt.Println("startBlock: ", startBlock)
	fmt.Println("endBlock: ", endBlock)
	fmt.Println("")
	dbResult, err := mysql.GetTransactionsByWallet(db, addr, startBlock, endBlock)
	if err != nil {
		panic(err)
	}
	var dbTxs []string
	for _, item := range dbResult {
		data := new(ethrpc.Transaction)
		json.Unmarshal([]byte(item.TransactionData), &data)
		fmt.Println("  ", data.Hash)
		dbTxs = append(dbTxs, data.Hash)
	}
	fmt.Println("")
	if !reflect.DeepEqual(dbTxs, correct_result) {
		t.Fatalf(`Result Incorrect.`)
	}
	fmt.Println("Result Match")
	fmt.Println("---------------")
	fmt.Println("")
}
