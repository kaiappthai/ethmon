package monitor

import (
	"database/sql"
	"encoding/json"
	"ethmon/ethrpc"
	"ethmon/mysql"
	"fmt"
)

func FetchTransactionsByAccount(addr string, startBlockNumber int, endBlockNumber int, returnJson bool, saveToDB bool) ([]MonitorResult, error) {
	var wallets []Wallet
	wallets = append(wallets, Wallet{
		Addr:       addr,
		StartBlock: startBlockNumber,
		EndBlock:   endBlockNumber,
	})
	request := MonitorRequest{
		Wallets:    wallets,
		ReturnJson: returnJson,
		SaveToDB:   saveToDB,
	}
	return FetchTransactionsByAccounts(request)
}

func FetchTransactionsByAccounts(req MonitorRequest) ([]MonitorResult, error) {
	var result []MonitorResult
	// check no. of wallet
	if len(req.Wallets) == 0 {
		return nil, fmt.Errorf("no wallet to monitor")
	}

	var db *sql.DB
	if req.SaveToDB {
		cdb, err := mysql.NewMySQLConnect()
		if err != nil {
			return nil, err
		}
		db = cdb
		defer db.Close()
	}

	// initialize ethrpc
	client := ethrpc.NewEthRPC()
	version, err := client.Web3ClientVersion()
	if err != nil {
		return nil, err
	}
	fmt.Println("Client version:", version)

	// get start/end block of all wallets to monitor
	blockNumber, err := client.EthBlockNumber()
	if err != nil {
		return nil, err
	}
	var startBlock int = int(blockNumber)
	var endBlock int = 0
	for _, wallet := range req.Wallets {
		if startBlock > wallet.StartBlock {
			startBlock = wallet.StartBlock
		}
		if endBlock < wallet.EndBlock {
			endBlock = wallet.EndBlock
		}
	}
	if startBlock == blockNumber {
		startBlock = 0
	}
	if endBlock == 0 {
		endBlock = int(blockNumber)
	}

	// fetch transaction and check address
	for i := startBlock; i <= endBlock; i++ {
		fmt.Println("Searching block ", i)
		block, err := client.EthGetBlockByNumber(i, true)
		if err != nil {
			return nil, err
		}
		for _, btx := range block.Transactions {
			raw, err := client.EthGetTransactionByHashRaw(btx.Hash)
			if err != nil {
				return nil, err
			}
			tx := new(ethrpc.Transaction)
			err = json.Unmarshal(raw, &tx)
			if err != nil {
				return nil, err
			}
			for _, wallet := range req.Wallets {
				if (wallet.StartBlock >= i || wallet.EndBlock <= i) && (wallet.Addr == tx.From || wallet.Addr == tx.To) {
					var flag string = "unknown"
					if wallet.Addr == tx.From {
						flag = "outgoing"
					}
					if wallet.Addr == tx.To {
						flag = "incomoing"
					}
					fmt.Println("  ", flag, "tx:", tx.Hash)

					data, err := json.Marshal(&raw)
					if err != nil {
						return nil, err
					}
					res := MonitorResult{
						Addr:             wallet.Addr,
						BlockNumber:      int(*tx.BlockNumber),
						TransactionIndex: int(*tx.TransactionIndex),
						TransactionData:  string(data),
					}
					if req.ReturnJson {
						result = append(result, res)
					}
					if req.SaveToDB {
						dbtx := mysql.Transaction{
							Addr:             res.Addr,
							BlockNumber:      res.BlockNumber,
							TransactionIndex: res.TransactionIndex,
							TransactionData:  res.TransactionData,
						}
						mysql.InsertTransaction(db, dbtx)
					}
				}
			}
		}
	}
	return result, nil
}
