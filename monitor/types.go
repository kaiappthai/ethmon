package monitor

type Wallet struct {
	Addr       string `json:"wallet"`
	StartBlock int    `json:"startBlock"`
	EndBlock   int    `json:"endBlock"`
}

type MonitorResult struct {
	Addr             string `json:"addr"`
	BlockNumber      int    `json:"blockNumber"`
	TransactionIndex int    `json:"transactionIndex"`
	TransactionData  string `json:"transactionData"`
}

type MonitorRequest struct {
	Wallets    []Wallet `json:"wallets"`
	ReturnJson bool     `json:"returnJson"`
	SaveToDB   bool     `json:"saveToDB"`
}
