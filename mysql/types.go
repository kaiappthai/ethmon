package mysql

type Config struct {
	Host       string `json:"db_host"`
	Port       int    `json:"db_port"`
	Database   string `json:"database"`
	User       string `json:"username"`
	Pass       string `json:"password"`
	UseSSL     bool   `json:"use_ssl"`
	CACert     string `json:"ca_cert"`
	ClientCert string `json:"client_cert"`
	ClientKey  string `json:"client_key"`
}

type Transaction struct {
	Addr             string `json:"addr"`
	BlockNumber      int    `json:"blockNumber"`
	TransactionIndex int    `json:"transactionIndex"`
	TransactionData  string `json:"transactionData"`
}
