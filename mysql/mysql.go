package mysql

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

func NewMySQLConnect() (*sql.DB, error) {
	config, err := GetConfig()
	if err != nil {
		return nil, err
	}
	if config.UseSSL {
		tlsConf := createTLSConf(config)
		err := mysql.RegisterTLSConfig("custom", &tlsConf)
		if err != nil {
			return nil, err
		}
	}
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", config.User, config.Pass, config.Host, config.Port, config.Database))
	if err != nil {
		return nil, err
	}
	return db, nil
}

func CheckDatabase() error {
	db, err := NewMySQLConnect()
	if err != nil {
		return err
	}
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS transaction (id INT NOT NULL AUTO_INCREMENT, addr VARCHAR(42) NOT NULL, blockNumber INT NULL DEFAULT 0, transactionIndex INT NULL DEFAULT 0, transactionData TEXT NULL, PRIMARY KEY (id))")
	if err != nil {
		return err
	}
	defer db.Close()
	return nil
}

func InsertTransaction(db *sql.DB, tx Transaction) {
	var rowno int
	db.QueryRow(fmt.Sprintf("SELECT count(id) FROM transaction WHERE addr = '%s' AND blockNumber = %d AND transactionIndex = %d", tx.Addr, tx.BlockNumber, tx.TransactionIndex)).Scan(&rowno)
	if rowno == 0 {
		insert, err := db.Query(fmt.Sprintf("INSERT INTO transaction (addr, blockNumber, transactionIndex, transactionData) VALUES ( '%s', %d, %d, '%s' )", tx.Addr, tx.BlockNumber, tx.TransactionIndex, tx.TransactionData))
		if err != nil {
			panic(err.Error())
		}
		defer insert.Close()
	}
}

func DeleteTransactionsByWallet(db *sql.DB, addr string, startBlock int, endBlock int) error {
	qryStartBlock := ""
	qryEndBlock := ""
	if startBlock > 0 {
		qryStartBlock = fmt.Sprintf(" AND blockNumber >= %d", startBlock)
	}
	if endBlock > 0 {
		qryEndBlock = fmt.Sprintf(" AND blockNumber <= %d", endBlock)
	}
	delete, err := db.Query(fmt.Sprintf("DELETE FROM transaction WHERE addr = '%s' %s%s", addr, qryStartBlock, qryEndBlock))
	if err != nil {
		return err
	}
	defer delete.Close()
	return nil
}

func GetTransactionsByWallet(db *sql.DB, addr string, startBlock int, endBlock int) ([]Transaction, error) {
	var transactions []Transaction
	qryStartBlock := ""
	qryEndBlock := ""
	if startBlock > 0 {
		qryStartBlock = fmt.Sprintf(" AND blockNumber >= %d", startBlock)
	}
	if endBlock > 0 {
		qryEndBlock = fmt.Sprintf(" AND blockNumber <= %d", endBlock)
	}
	results, err := db.Query(fmt.Sprintf("SELECT * FROM transaction WHERE addr = '%s' %s %s ORDER BY blockNumber, transactionIndex ", addr, qryStartBlock, qryEndBlock))
	if err != nil {
		return nil, err
	}
	for results.Next() {
		var tx Transaction
		var id int
		err = results.Scan(&id, &tx.Addr, &tx.BlockNumber, &tx.TransactionIndex, &tx.TransactionData)
		if err != nil {
			return nil, err
		}
		transactions = append(transactions, tx)
	}
	return transactions, nil
}
