package mysql

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

func GetConfig() (*Config, error) {
	jsonFile, err := os.Open("mysql/config.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	byteValue, _ := io.ReadAll(jsonFile)
	config := new(Config)
	if err := json.Unmarshal(byteValue, &config); err != nil {
		return config, err
	}
	return config, nil
}

func createTLSConf(config *Config) tls.Config {
	rootCertPool := x509.NewCertPool()
	pem, err := os.ReadFile(config.CACert)
	if err != nil {
		log.Fatal(err)
	}
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}
	clientCert := make([]tls.Certificate, 0, 1)

	certs, err := tls.LoadX509KeyPair(config.ClientCert, config.ClientKey)
	if err != nil {
		log.Fatal(err)
	}

	clientCert = append(clientCert, certs)

	return tls.Config{
		RootCAs:            rootCertPool,
		Certificates:       clientCert,
		InsecureSkipVerify: true, // needed for self signed certs
	}
}
