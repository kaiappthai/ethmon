package main

import (
	"ethmon/api"
	"ethmon/ethrpc"
	"ethmon/mysql"
)

func main() {
	err := mysql.CheckDatabase()
	if err != nil {
		panic(err)
	}
	err = ethrpc.CheckEthRPC()
	if err != nil {
		panic(err)
	}
	api.StartAPIServer()
}
