package api

import (
	"ethmon/ethrpc"
)

type Config struct {
	APIPort int    `json:"api_port"`
	APIPath string `json:"api_path"`
}

type MonitorResult struct {
	Result bool                 `json:"result"`
	Items  []ethrpc.Transaction `json:"items"`
}

type ResultError struct {
	Result bool   `json:"result"`
	Error  string `json:"error"`
}

type ResultSuccess struct {
	Result bool `json:"result"`
}

type TransactionsByWalletRequest struct {
	Addr       string `json:"addr"`
	StartBlock int    `json:"startBlock"`
	EndBlock   int    `json:"endBlock"`
}
