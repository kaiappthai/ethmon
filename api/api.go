package api

import (
	"fmt"
	"net/http"
	"os"
)

func StartAPIServer() {
	config, err := getConfig()
	if err != nil {
		panic(err)
	}
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	http.HandleFunc(fmt.Sprintf("%s/fetchTransactionsByAccounts", config.APIPath), fetchTransactionsByAccounts)
	http.HandleFunc(fmt.Sprintf("%s/transactionsByAccount", config.APIPath), transactionsByAccount)

	fmt.Printf("Start API on %s port %d\n", hostname, config.APIPort)
	http.ListenAndServe(fmt.Sprintf(":%d", config.APIPort), nil)
}

func fetchTransactionsByAccounts(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		fetchTransactionsByAccounts_Post(w, r)
	} else {
		w.WriteHeader(405)
	}
}

func transactionsByAccount(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		getTransactionsByAccount(w, r)
	} else if r.Method == "DELETE" {
		deleteTransactionsByAccount(w, r)
	} else {
		w.WriteHeader(405)
	}
}
