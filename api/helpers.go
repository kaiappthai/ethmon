package api

import (
	"encoding/json"
	"ethmon/ethrpc"
	"ethmon/monitor"
	"ethmon/mysql"
	"fmt"
	"io"
	"net/http"
	"os"
)

func getConfig() (*Config, error) {
	jsonFile, err := os.Open("api/config.json")
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()
	byteValue, _ := io.ReadAll(jsonFile)
	config := new(Config)
	if err := json.Unmarshal(byteValue, &config); err != nil {
		return config, err
	}
	return config, nil
}

func returnAccept(w http.ResponseWriter, ret MonitorResult) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ret)
}

func returnAcceptSuccess(w http.ResponseWriter, ret ResultSuccess) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ret)
}

func returnReject(w http.ResponseWriter, err error) {
	ret := ResultError{}
	ret.Result = false
	ret.Error = err.Error()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ret)
}

func fetchTransactionsByAccounts_Post(w http.ResponseWriter, r *http.Request) {
	req := new(monitor.MonitorRequest)
	req.ReturnJson = true
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		returnReject(w, err)
		return
	}
	result, err := monitor.FetchTransactionsByAccounts(*req)
	if err != nil {
		returnReject(w, err)
		return
	}
	var txs = []ethrpc.Transaction{}
	for _, res := range result {
		tx := new(ethrpc.Transaction)
		err = json.Unmarshal([]byte(res.TransactionData), &tx)
		if err != nil {
			returnReject(w, err)
			return
		}
		txs = append(txs, *tx)
	}
	ret := MonitorResult{}
	ret.Result = true
	ret.Items = txs
	returnAccept(w, ret)
}

func getTransactionsByAccount(w http.ResponseWriter, r *http.Request) {
	req := new(TransactionsByWalletRequest)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		returnReject(w, err)
		return
	}
	db, err := mysql.NewMySQLConnect()
	if err != nil {
		returnReject(w, err)
		return
	}
	defer db.Close()
	result, err := mysql.GetTransactionsByWallet(db, req.Addr, req.StartBlock, req.EndBlock)
	if err != nil {
		returnReject(w, err)
		return
	}
	var txs = []ethrpc.Transaction{}
	for _, item := range result {
		data := new(ethrpc.Transaction)
		err = json.Unmarshal([]byte(item.TransactionData), &data)
		if err != nil {
			returnReject(w, err)
			return
		}
		txs = append(txs, *data)
	}
	ret := MonitorResult{}
	ret.Result = true
	ret.Items = txs
	returnAccept(w, ret)
}

func deleteTransactionsByAccount(w http.ResponseWriter, r *http.Request) {
	req := new(TransactionsByWalletRequest)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		returnReject(w, err)
		return
	}
	db, err := mysql.NewMySQLConnect()
	if err != nil {
		returnReject(w, err)
		return
	}
	defer db.Close()
	err = mysql.DeleteTransactionsByWallet(db, req.Addr, req.StartBlock, req.EndBlock)
	if err != nil {
		returnReject(w, err)
		return
	}
	ret := ResultSuccess{}
	ret.Result = true
	returnAcceptSuccess(w, ret)
}
